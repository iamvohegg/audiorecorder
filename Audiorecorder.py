#!/usr/bin/env python3
# Work in progress
"""
Start a sound playing website (I decide to spotify web)
this recorder collect the information from the browser-title
and record a wav file, which is directly turn into a mp3 file

requirements:
to capture Audio from the Soundcard there are a number of thinks to - be there -
mostly they are in the repro.
So first check that. Here a way how to do that:
Start any type of music play with your machine , open a console and type:
    arecord -d 10 demo.wav
this should run without error a record a 10' second wave file.
If this fail, install arecord:
    sudo apt-get install arecord -f
In my case the record almost graps the microfon and not the line in channel of the soundcard.
If to adjust this use a mixer or volume control software. (yes there is a way to do this)
with arecord direct but that sucks (I guess)
So install the simplest way to check the audiochannels (I know at this time)
    sudo apt-get install pavucontrol
Now check this together:
Run an audio playback again (may a YouTube video or your favorit music file)
start arecord demo1.wav  (for recording without any ending)
start pulsaudiovolumecontroler (pavucontrol)
On the playback-tab you should see your playback as volume-meter
go to the input tab and look out for a monitor-setting it should peek out a volume-meter too.
under the recording-tab a volume-meter peeking out and there is a setting which
must be set to - monitor of internal.... - (instead of internal audio )
Fine.
Stop all this (may with ctrl-c or alt-F4 key's)
At last check if lame is installed.
At this time the Audioreorder uses recording in wav, streaming that to the mp3 converter lame.
I think of doing recording and converting in two separate threads, so the latency is small
and recording has no lagging.

"""
__author__ = "Volker Heggemann"
__copyright__ = "Copyright 2019, Volker Heggemann"
__credits__ = ["The PySimpleGui Project", "The itunepy Project"]
__license__ = "CC BY-NC-SA"
__version__ = "3.05 DE"
__maintainer__ = "vohe"
__email__ = "vohegg@gmail.com"
__status__ = "rolling Release"

# Imports global libs
# for CLI - Parameters we pharse Arguments
import argparse
# because of open subprocesses it is important do close (kill) that at the exit of this program
# so with this out own exit procedure is added
import atexit
# localisation should be possible. in this case gettext is used to load the locales from files
import gettext
import glob
# for logging to file or stdout this is given.
# uses this instead of print something out to stdout!
import logging
# at the end there is a list of soundfiles which should be tagged and gained
# this operation takes quite a long time
# I implement this as I use ffmpeg and replaygain for these jobs.
# Now I use mp3gain and musictag, but it gives such an atvantage to do in with more than one process
# working with path, files, os.separators etc.
import os
# may in the future, in a far, far galaxy, it is possible to run arecord,mp3gain,lame and
# under windows and mac!? In this case, I could decide to do a crossplatform programming ;-)
import platform
# Signal is need to get process id's (PID) just for kill them
import signal
# I need to bend stdout
import sys
from configparser import ConfigParser
# At the begining of the logging I want to pastime in the file
from datetime import datetime
from shutil import which
from subprocess import Popen, PIPE

# What a simple, loveley gui framework
# credits to : https://github.com/PySimpleGUI/PySimpleGUI
import PySimpleGUI as Sg
# get the users appdir for write logfiles
# credit to : https://github.com/ActiveState/appdirs
# import xdgappdirs
# musictagger modul, works as I should
# credit to : https://github.com/KristoforMaynard/music-tag
import music_tag
# Cover Art finder
# credit to : https://github.com/regosen/get_cover_art
from get_cover_art import CoverFinder
# I wrote a lot of  moduls for Lyrics. I look for a thing that fetch lyrics without the need of an api Key, or login
# from lyricstagger import lyrics_for
# credit to : hey that's mine
from pynormalize import pynormalize
import xdgappdirs
from lyricsdownloader import lyrics_for
# ok that is where we look for an album for a given song and artist
# there a much more information to get, but at the moment I do not use that's
# credit to : hey that's mine
from onlinetagger import get_albumname
# this modul looks for an open browserwindow.
# if found, it got title and artist from the windowtitle
# credit to : hey that's mine
from openwindows import title_artist_from_browser
import slugify

# hey, this is an awesome modul for create a GUI in some simple lines.

# eventlooptime is used by PySimpleGui for leaving the window-loop
# shorten this if your computer is busy
# I use 400 on my old thinkpad T430 (2CPU,max. RAM)
eventlooptime = 200
debuglevel = logging.CRITICAL
debug_console = True

if debug_console:
    logging.basicConfig(stream=sys.stdout, level=debuglevel)
else:
    logging.basicConfig(filename="Audiorecorder.log", level=debuglevel)

logging.info(f'\n\nStarting Audiorecoder {datetime.now()}\n')
# logging.debug('This message should go to the log file')
# logging.info('So should this')
# logging.warning('And this, too')
# logging.error('And non-ASCII stuff, too, like Øresund and Öffi')

lang = ('de', 'en', 'nd')
almanach = {}

try:
    for language in lang:
        almanach[language] = gettext.translation('Audiorecorder', localedir='locales', languages=[language])
except LookupError as e:
    logging.warning(f'Error in translation {e}')


    def _(s):
        return s
    # better than this (pep8)
    # _ = lambda s: s


# Class declarations
# ok,lets start this....
# another recorder

class MyRecorder:
    """
    :return: a string
    """
    normalize: float

    # lamepath isn't set at all, normaly it is in the OS-Path
    # outpath = if not set, this is the home/Music_Rec directory of user
    # genere = set's the musik genere to a string
    # subfolder =  if True, the ..outpath/ changes to ..outpath/genere/
    # mp3tag = if True, the mp3tagger started at the end of program to tag all files
    # quality = set to 1 or 2 - which goes to lame -V1 or -V2 ( V1 is default) - mp3 bitrate up to 190 or 320
    # counter = if True, all Filenames are numbered decending from 001 up to end. (this is for recording Audiobooks)
    #

    def __init__(self, lamepath, outpath, genere, subfolder, mp3tag, quality, counter=False, notification=False,
                 locale_set='en', gain=True):
        self.normalize_offset = None
        self.window = None
        global lang
        global almanach
        # this ascii will rotate in the statusline
        self.spinnerstring = '⢎⡰⢎⡡⢎⡑⢎⠱⠎⡱⢊⡱⢌⡱⢆⡱'
        # here is the aktual string for that
        self.status_spinner = ''
        # so put the given values in the class variables
        self.lame_path = lamepath
        self.outpath = outpath
        self.genere = genere
        if quality == '1':
            self.quality = '-V1'
        elif quality == '2':
            self.quality = '-v2'
        else:
            self.quality = '-V1'
        if genere:
            self.musicpath = os.path.join(self.outpath, self.genere)
        else:
            self.musicpath = self.outpath
        self.subfolder = subfolder
        self.mp3tag = mp3tag
        self.mp3gain = gain
        self.normalize = 89.0
        self.counttitles = counter
        self.notification = notification
        self.filepath = ''
        # hey, there is a lastfm online tagger! but I do not use that anymore.
        # now I use the itunepy apikey
        self.imagepath = os.path.dirname(os.path.abspath(__file__))

        # as we use arecord to get out audio stream into a mp3, we start and end processes
        # if we forget to end this processes, our machine go out of RAM and Processtime
        # so (for the moment) we collect all started Processes in a list of ID's
        self.all_pids = []
        # for tagging all the mp3, we need information about them
        # and to avoid less (sound) recording quality, we tag all the files at the end of
        # our recording session
        # in this case, we collect all the information about the files in a list of dicts
        # think of this like a Table.
        self.all_files = []
        self.temp_files = []
        # our titlecounter (see. self.counttitles) starts at zero
        self.title_counter = 0
        # some things - like kill all processes or tag all files should just
        # happen one time.
        # here we set a flag for the exit of our class
        self.happened = False
        self.possible_recording = False
        self.started = True
        # the recording loop needs some aktual variables
        self.title_akt = ''
        self.artist_akt = ''
        self.title_old = ''
        self.artist_old = ''
        self.album_akt = ''
        self.id_akt = -1
        self.id_last = -1
        # if something is going wrong, like an ioerror we didn't fetch, there is an exitpoint
        #
        atexit.register(self.my_cleanup)
        # possible languarges
        self.lang_akt = locale_set
        self.td = {}
        self.td2 = {}
        logging.info(f'Language is {self.lang_akt}')
        self.ini_dir = 'Audiorecorder'
        self.inifilename = self.ini_dir + '.ini'
        self.inifilepath = xdgappdirs.user_state_dir(self.ini_dir, 'V1.0', '')
        self.configfile = ConfigParser()
        self.read_config()
        if os.path.isfile(self.inifilepath + os.sep + self.inifilename):
            self.configfile.read(self.inifilepath, encoding='utf-8')
        else:
            self.parser_write()

    def parser_write(self):
        """
        :return: zero
        """
        if not os.path.exists(self.inifilepath):
            os.makedirs(self.inifilepath)
        with open(self.inifilepath + os.sep + self.inifilename, 'w') as cf:
            self.configfile.write(cf)
        return 0

    def save_conifg(self):
        """
        :return: nothing
        """
        if not self.configfile.has_section('path'):
            self.configfile.add_section('path')
        self.configfile.set('path', 'lamepath', self.lame_path)
        self.configfile.set('path', 'outpath', self.outpath)
        self.configfile.set('path', 'musicpath', self.musicpath)
        if not self.configfile.has_section('options'):
            self.configfile.add_section('options')
        if self.genere:
            self.configfile.set('options', 'genere', self.genere)
        if self.quality:
            self.configfile.set('options', 'quality', self.quality)
        self.configfile.set('options', 'subfolder', str(self.subfolder))
        self.configfile.set('options', 'mp3tagging', str(self.mp3tag))
        self.configfile.set('options', 'mp3tagging', str(self.mp3tag))
        self.configfile.set('options', 'mp3gianing', str(self.mp3gain))
        self.configfile.set('options', 'mp3normalize', str(self.normalize))

        # self.counttitles = counter
        # self.notification = notification
        # self.filepath = ''

        self.parser_write()

    def read_config(self):
        """
        :retrun: nothing
        """
        # if no configpath is there, then nothing to read
        if not os.path.exists(self.inifilepath):
            return
        # if path is there, but file isn't then nothing to read
        if not os.path.isfile(self.inifilepath + os.sep + self.inifilename):
            return
        self.configfile.read(self.inifilepath + os.sep + self.inifilename)
        if self.configfile.has_section('path'):
            self.lame_path = self.configfile.get('path', 'lamepath', fallback=self.lame_path)
            self.outpath = self.configfile.get('path', 'outpath', fallback=self.outpath)
            self.musicpath = self.configfile.get('path', 'musicpath', fallback=self.musicpath)
        if self.configfile.has_section('options'):
            self.genere = self.configfile.get('options', 'genere', fallback=self.genere)
            self.quality = self.configfile.get('options', 'quality', fallback=self.quality)
            self.subfolder = self.configfile.get('options', 'subfolder', fallback=self.subfolder) == 'True'
            self.mp3tag = self.configfile.get('options', 'mp3tagging', fallback=self.mp3tag) == 'True'
            self.mp3gain = self.configfile.get('options', 'mp3gianing', fallback=self.mp3gain) == 'True'
            self.normalize = float(self.configfile.get('options', 'mp3normalize', fallback=self.normalize))
        # for tagging all mp3 files with Title, Album, Artist (if lame has failed to do that)
        # here, we run through all the files recorded and call the mp3 tagger
        # may there is another lib that does more than lame?
        # additionally, as all information are at this point,
        # it is possible to do e.g. a replaygain here
        #

    @staticmethod
    def get_tags_from_file(filepath):
        try:
            f = music_tag.load_file(filepath)
            title = str(f["title"])
            if len(title.strip()) < 1:
                title = str(f["tracktitle"]).strip()
            artist = str(f["artist"]).strip()
            album = str(f["album"]).strip()
            if len(artist.strip()) < 1:
                artist = str(f["album artist"]).strip()
            if len(album.strip()) < 1:
                album = get_albumname(given_artist=artist, given_song=title, without='')
        except (KeyError, TypeError, Exception) as err:
            logging.warning(f'Error while get tag from files {err}')
            title = ""
            artist = ""
            album = ""
        return {"filename": filepath, "title": title, "artist": artist,
                "album": album, "path": filepath}

    @staticmethod
    def tagging(item=None):
        """
        :return: True if success
        """
        if item is None:
            item = {}
        linefile = os.path.realpath(item["path"])
        if os.path.isfile(linefile):
            try:
                f = music_tag.load_file(linefile)
                f["album artist"] = item["artist"]
                f["artist"] = item["artist"]
                f["album"] = item["album"]
                f["tracktitle"] = item["title"]
                f["title"] = item["title"]
                f["lyrics"] = lyrics_for(song_title=item["title"], song_artist=item["artist"])
                f.save()
            except Exception as error:
                logging.warning(f'Tagging error {error} file {item["path"]}')
                pass
            return True

        # may the user changed the gain to another value, so everytime we want to use the dboffset, we have to
        # recalculate that

    def get_normalize_offset(self):
        self.normalize_offset = 100.0 - self.normalize
        return self.normalize_offset

    def gaining(self, item):
        # pynormalize file and dboffset
        linefile = os.path.realpath(item["path"])
        try:
            if os.path.isfile(linefile):
                pynormalize.process_files([linefile], self.get_normalize_offset())
        except Exception as error:
            logging.warning(f'Gaining error {error}')
            pass

        # another gainig method not used, because of better one
        # swap naming of gainingX and gaining to use it.

    def gaining2(self, item):
        # mp3gain -r -k -d 2 -p *.mp3
        linefile = os.path.realpath(item["path"])
        # cmd_params = f'mp3gain -r -k -m {int(self.normalize - 87.0)} "{linefile}"'
        cmd_params = f'mp3gain -r -k -d {int(self.normalize - 87.0)} "{linefile}"'
        try:
            if os.path.isfile(linefile):
                Popen(cmd_params, bufsize=1, shell=True)
        except Exception as error:
            logging.warning(f'Gaining error {error}')
            pass

    def tag_and_gain_all_files(self, thisfiles, tagging=False, gaining=False):
        """
        this walk throu the recorded files (mostly mp3 files) tag and normalize them through a value that's given
        at the moment I set max dB level -5.0 which mean very loud at 95 dB
        first I do this with a popen call of mp3gain, which has to be installed in the system. Now I use sox,
        with a temporary file named sox.mp3.
        If you found a file like that, you could delete it, but it is an indicator for an interrupted normalization!
        :return: nothing
        """

        if not tagging and not gaining:
            return

        if thisfiles:
            finder = CoverFinder({'--art-dest-inline': True, '--external-art-filename': 'cover.jpg', '--force': True})
            sum_of_files = len(thisfiles)

            cnt = 0
            for line in thisfiles:
                cnt += 1
                if tagging:
                    # I want to translate that, so unfortunately no f-string here
                    self.set_statusbar_text(
                        _('Tagging {} of {} {}').format(cnt, sum_of_files, line["title"]))
                    self.tagging(line)
                if gaining:
                    self.set_statusbar_text(_('Gain {} of {} {} to {} dB').format(cnt, sum_of_files,
                                                                                  line["title"], self.normalize))
                    self.gaining2(line)
                linefile = os.path.realpath(line["path"])
                if os.path.isfile(linefile):
                    self.set_statusbar_text(
                        _('Attach cover to {} - {} of {}').format(line["title"], cnt, sum_of_files))
                    finder.scan_file(linefile)
            logging.info(' ')

        # once a time, our class goes
        # there is to do some sweeping and cleaning

    def my_cleanup(self):
        """
        :return: a string
        """
        # do this only one time
        # this is nessesary because the atexit() function could call this twice!!
        if self.happened:
            return
        self.happened = True
        logging.info("Cleaning up...")
        logging.info(f'Mp3Tagger {self.mp3tag}')
        logging.info(f'Gain files {self.mp3gain}')
        self.tag_and_gain_all_files(self.all_files, tagging=self.mp3tag, gaining=self.mp3gain)
        # if some arecoding sessions already opened? Then kill them
        kill_all_arecords()

        # make Audioenhancements manuell

        # in this case there should be a list of (Audio-)files to proceed
        # and a set of funtions to throw them throu

    @staticmethod
    def getfiles(basedir, extension):
        return glob.iglob(f"{basedir}/**/{extension}", recursive=True)

    def audioenhance_man(self):
        """

        @rtype: object
        """
        start_path = Sg.popup_get_folder(message=_("Select or change the music path"),
                                         title=_("Please choose a folder"),
                                         default_path=self.musicpath, initial_folder=self.musicpath, )
        self.set_statusbar_text(_('Startpath: {}').format(start_path))
        if start_path:
            if len(start_path) > 1 and os.path.isdir(start_path):
                self.set_statusbar_text(_("Collection Files - takes a while."))
                name = self.getfiles(start_path, '*.mp3')
                for soundfile in name:
                    if os.path.isfile(soundfile):
                        item = self.get_tags_from_file(soundfile)
                        self.temp_files.append(item)
                        self.set_statusbar_text(f'{self.status_spinner}'.join(
                            _('Found: {}\n Title: {}  \t Artist: {} \t on Album: {}').format(item["path"],
                                                                                             item["title"],
                                                                                             item["artist"],
                                                                                             item["album"])))
                old = self.started
                self.started = False
                self.tag_and_gain_all_files(self.temp_files, tagging=self.mp3tag, gaining=self.mp3gain)
                self.started = old

        self.set_statusbar_text(f'{self.status_spinner}'.join(_("Done. Ready to take new instructions.")))

        # here is the main recording method
        #

    def record(self, soundfilename):
        """
        :return: a pid
        """
        # we will start an os.subprocess! so if we don't store his number,
        # it's impossible to end the subprocess later!
        # subprocesses got ID' called Process ID'S or PID ;-)
        intern_id = 0
        if platform.system() == "Linux" or platform.system() == "Linux2":
            # if it's the wish to store the mp3 in a folder like
            # home/Music_Rec/Rock/Queen/Somebody to love.mp3
            # instead of
            # home/Music_Rec/Rock/Somebody to love.mp3
            # we glue this here together.
            if self.subfolder:
                self.filepath = os.path.join(self.musicpath, self.artist_akt, soundfilename.strip())
            else:
                self.filepath = os.path.join(self.musicpath, soundfilename.strip())
            # what if the path isn't there
            # or only a part of the path is there?
            # so try to create the full path
            try:
                os.makedirs(os.path.dirname(self.filepath))
            except FileExistsError:
                pass
            # now, letz record , that's easy
            # you could type this in a linux console, and it'll record your sound
            #
            # be shure, that your sound is playing
            # I found out, under ubuntu 19.xx I could set volume to zero but if I mute the sound
            # nothing is recorded
            # check this for your self. May install
            # under debian/ubuntu : sudo apt-get install pavucontrol
            # arecord gives us a pure wav stream
            arecord_proc = Popen('arecord -q -f cd'.split(), stdout=PIPE)
            # we pipe this direct to lame (check if lame is in your system!)
            # type : lame in your console
            #
            # to debug this use : lame_proc = subprocess.Popen(
            Popen(
                ["lame", self.quality, "--noreplaygain", "--quiet", "--tl", self.album_akt, "--ta", self.artist_akt,
                 "--tt",
                 self.title_akt, '-',
                 self.filepath],
                stdin=arecord_proc.stdout,
                stdout=PIPE)
            # and off it goes, recorder work for now till we kill it
            # this is a commandline like:
            # arecord -q -f cd | lame -V1 --noreplaygain --quiet --tl album --ta artist_akt --tt title_akt - filepath
            #
            intern_id = arecord_proc.pid
        return intern_id

        # after the Class is init
        # and may the tagger (setup_lastfm() ) is init
        # this is the main start point

    @staticmethod
    def get_locale_text():
        """
        :return: a string
        """
        tdict = {"k_onair": _("On Air"), "viewtxt": _("View or change actual settings"), "localetxt": _("Locale"),
                 "tab1": _("Actual recording"), "tab2": _("Setting"), "outpathtxt": _("Outpath\t"),
                 "generetxt": _("Genere\t"), "qualitytxt": _("Quality (V1/V2)\t"), "subfoldertxt": _("Subfolder"),
                 "mp3gain": _("Gain to Value dB (MP3Gain/SOX)"), "Gain-Level": _("Normalize !Stop at 94dB!"),
                 "mp3tagtxt": _("Mp3 tagger"), "notifytxt": _("System notifikation"), "k_title": _("Title: \t"),
                 "k_artist": _("Artist: \t"), "k_album": _("Album found: \t"), "k_song": _("Song: \t"),
                 "Manually": _("Tag/Gain/... by Hand"),
                 "Exit": _("Exit"),
                 "Stop": _("Stop"),
                 "Save": _("Save"),
                 "Start": _("Start"),
                 "tab3": _("About"),
                 "abouttext": _("This is brought to you by vohegg (at) gmail.com \n Only for privat use, "
                                "programmed as case study"),
                 "donate": _("Like that ? Donate it, buy me a book! \nhttps://www.buymeacoffee.com/vohegg")}
        return tdict

    @staticmethod
    def get_locale_notify():
        """
        :return: a string
        """
        tdict2 = {"notify1": _("Audiorecorder - A new Title pending"),
                  "notify2": _("Artist\t: {}\nTitle\t: {}\nAlbum\t: {}"),
                  "radiotooltip": _("Title information comes back with the next song"),
                  "Save": _("Save a Configuration File. Look at: {}"),
                  "Start": _("Starts recording - after stopping it. Normaly Autostart is active."),
                  "Stop": _(
                      "Stops recording. But beware! No tagging or gaining is done yet. You must push exit to do that!"),
                  "Manually": _("Enable this by close the player"),
                  }
        return tdict2

    def set_statusbar_text(self, newtext):
        if self.window:
            if self.window["Status"]:
                self.window["Status"].update(newtext)
                self.window.refresh()

    def startrecord(self):
        """
        empty
        """
        # reset the list of all Process ID's
        # look at record() method
        self.all_pids = []

        almanach[self.lang_akt].install()
        self.td = self.get_locale_text()
        self.td2 = self.get_locale_notify()

        # make a small window show what's going on
        Sg.theme("DarkAmber")  # "Default", "Default1", "DefaultNoMoreNagging" "SystemDefault", "SystemDefault1",
        # 'SystemDefaultForReal'

        tab1_layout = [
            [Sg.Button(button_text=self.td["k_onair"], key="k_onair", button_color=("white", "red"), disabled=True,
                       visible=False,
                       size=(10, 1))],
            [Sg.Text(size=(65, 1), key="k_title")],
            [Sg.Text(size=(65, 1), key="k_artist")],
            [Sg.Text(size=(65, 1), key="k_song")],
            [Sg.Text(size=(65, 1), key="k_album")],
            [Sg.VPush(),
             Sg.Button(button_text=self.td["Start"], tooltip=self.td2["Start"], key="Start"),
             Sg.Button(button_text=self.td["Stop"], tooltip=self.td2["Stop"], key="Stop"), ],
        ]

        tab2_layout = [
            [Sg.T(self.td["viewtxt"], key="viewtxt")],
            [Sg.T(self.td["localetxt"], key="localetxt", size=(16, 1))],
            [Sg.Button(choice, tooltip=self.td2["radiotooltip"]) for choice in lang],
            [Sg.Text(size=(40, 1), key="-RedLightOn-")],
            [Sg.T(self.td["outpathtxt"], size=(16, 1), key="outpathtxt"), Sg.Push(),
             Sg.Push(),
             Sg.FolderBrowse(button_text="...",
                             initial_folder=self.outpath,
                             target="k_outpath",
                             key="k_outbrowse"),
             Sg.Input(default_text=self.outpath, enable_events=True, key="k_outpath"),
             ],
            [Sg.T(self.td["generetxt"], size=(16, 1), key="generetxt"),
             Sg.Push(),
             Sg.Input(default_text=self.genere, enable_events=True, key="k_genere")],
            [Sg.T(self.td["qualitytxt"], size=(16, 1), key="qualitytxt"),
             Sg.Push(),
             Sg.Input(default_text=self.quality, enable_events=True, key="k_quality")],
            [Sg.T(self.td["subfoldertxt"], size=(30, 1), key="subfoldertxt"),
             Sg.Checkbox(text='', default=self.subfolder, change_submits=True, enable_events=True, key="k_subfolder"),
             Sg.Push(),
             Sg.T(self.td["mp3tagtxt"], size=(30, 1), key="mp3tagtxt"),
             Sg.Checkbox(text='', default=self.mp3tag, change_submits=True, enable_events=True, key="k_mp3tag")],
            [Sg.T(self.td["notifytxt"], size=(30, 1), key="notifytxt"),
             Sg.Checkbox(text='', default=self.notification, change_submits=True, enable_events=True, key="k_notify"),
             Sg.Push(),
             Sg.T(self.td["mp3gain"], size=(30, 1), key="mp3gain"),
             Sg.Checkbox(text='', default=self.mp3gain, change_submits=True, enable_events=True, key="k_mp3gain")
             ],
            [Sg.T(self.td["Gain-Level"], size=(len(self.td["Gain-Level"]), 1), key="Gain-Level"),
             Sg.Push(),
             Sg.Slider(range=(0.0, 100.0), orientation="horizontal", default_value=self.normalize,
                       enable_events=True, key="k_norm"),
             ],
        ]

        tab3_layout = [
            [Sg.T(self.td["abouttext"], key="abouttext")],
            [Sg.T(self.td["donate"], key="donate")],

            [Sg.Image(os.path.join(self.imagepath, "AudioCassette.png"), size=(300, 200))],
        ]

        layout = [
            [Sg.TabGroup([[
                Sg.Tab(self.td["tab1"], tab1_layout, key="tab1"),
                Sg.Tab(self.td["tab2"], tab2_layout, key="tab2"),
                Sg.Tab(self.td["tab3"], tab3_layout, key="tab3")

            ]], tooltip=_("Audiorecoder"))
            ],
            [Sg.Button(button_text=self.td["Manually"], key="Manually", disabled=self.possible_recording,
                       tooltip=self.td2["Manually"]),
             Sg.Button(button_text=self.td["Save"], tooltip=self.td2["Save"].format(self.inifilepath), key="Save"),
             Sg.Push(),
             Sg.Button(button_text=self.td["Exit"], key="Exit")
             ],
            [Sg.StatusBar(text="Status:", key="Status", expand_x=True, expand_y=True, size=(80, 2),
                          auto_size_text=True)]
        ]

        # this shows a window with a title and
        # four textlines to access later
        # below there is an exit button
        self.window = Sg.Window("Audiorecorder", layout, icon="./AudioCassette.png", resizable=True)
        # a loop till ever collect the button events
        flash = True
        shifter = 0
        count = 0
        spinner_point = 0
        audible = False
        aktive_pid = None

        while True:  # Event Loop
            # eventlooptime is like a tkinter.after() function
            # eventlooptime in milliseconds jump out the window class
            # give us time to react
            event, values = self.window.read(eventlooptime)
            # Sg.Button give there name as event
            spinner_point += 2
            if spinner_point >= len(self.spinnerstring):
                spinner_point = 0
            self.status_spinner = self.spinnerstring[spinner_point:spinner_point + 2]
            self.set_statusbar_text(f'{self.status_spinner}')
            if event in (None, "Exit"):
                self.my_cleanup()
                break
            if event in "Save":
                self.save_conifg()
            if event in "Start":
                self.started = True
            if event in "Stop":
                self.started = False
                self.set_statusbar_text(f'{self.status_spinner}'.join(_("Stopping, wait for end of title")))
            shifter += 1
            if shifter == 6:
                shifter = 0
                flash = not flash
                self.window["k_onair"].update((" ", "On Air")[flash], visible=(False, True)[flash and self.started])
                # hey this is only called shifter-times-loops, in this case we do upate other buttons too!
                self.window["Manually"].update(disabled=(False, True)[self.possible_recording])
            # check all the states of the settingsdialog, while we are not in recording mode!
            # update vars with new/changed settings?

            if event in ("k_outpath", "k_outbrowse"):
                # logging.info('Event outpath')
                if os.path.exists(values["k_outpath"]):
                    self.outpath = values["k_outpath"]
                    if self.genere:
                        self.musicpath = os.path.join(self.outpath, self.genere)
                    else:
                        self.musicpath = self.outpath
            if event in "k_locale":
                logging.info("Event locale")
            if event in "k_genere":
                logging.info("Event genere")
                self.genere = values["k_genere"]
                self.musicpath = os.path.join(self.outpath, self.genere)
            if event in "k_quality":
                logging.info("Event quality")
                if values["k_quality"] in ("V1", "1", "high"):
                    self.quality = "V1"
                elif values["k_quality"] in ("V2", "2", "low"):
                    self.quality = "V2"
            if event in "k_subfolder":
                logging.info("Event subfolder")
                self.subfolder = values["k_subfolder"]
            if event in "k_mp3tag":
                self.mp3tag = values["k_mp3tag"]
                logging.info(f"Event mp3 tagger {self.mp3tag}")

            if event in "k_mp3gain":
                self.mp3gain = values["k_mp3gain"]
                logging.info(f'Event mp3 gain {self.mp3gain}')
            if event in "k_norm":

                if values["k_norm"] > 96.0:
                    if Sg.popup_ok_cancel(_("Beware of klipping!"), modal=True, keep_on_top=True):
                        self.normalize = values["k_norm"]
                else:
                    self.normalize = values["k_norm"]

                self.window["k_norm"].update(self.normalize)
            if event in "k_notify":
                logging.info("Event notiify")
                self.notification = values["k_notify"]
            if event in lang:
                almanach[event].install()
                self.td = self.get_locale_text()
                self.td2 = self.get_locale_notify()
                for key in self.td:
                    element = self.window[key]
                    try:
                        if self.td2[key]:
                            try:
                                if self.td[key]:
                                    element.set_tooltip(self.td2[key])
                            except KeyError:
                                pass
                    except KeyError:
                        pass
                    if isinstance(element, (Sg.Text, Sg.Frame, Sg.Multiline, Sg.StatusBar)):
                        element.update(value=self.td[key])
                    if isinstance(element, (Sg.Button, Sg.Checkbox)):
                        element.update(text=self.td[key])
                    if isinstance(element, Sg.Tab):
                        element.update(title=self.td[key])

            if event in "Manually":
                logging.info("New Feature Manual Enhancement")
                self.audioenhance_man()

            self.window.refresh()
            #
            # look if there is a browserwindow with grabable title
            # browser is not given, but we could send a 'Mozilla Firefox' or 'Google Chrome'
            # it gives uns title, artist and the boolean found back if it's successful

            title, artist, found, browser, aktive_pid = title_artist_from_browser(pid=aktive_pid)
            title_set = ''
            if not audible and artist == 'Audible':
                title_set = slugify.slugify(Sg.popup_get_text(
                    'Press Pause and rewind to start then enter title and start player',
                    title='Audible Cloud Player found!'))
                # browser = 'Firefox'
                audible = True
            if audible and artist == 'Audible':
                title = title_set
            count += 1
            if found:
                # cause this is a loop, may we get this information a last time?
                if (title != self.title_akt) or (artist != self.artist_akt):

                    self.possible_recording = False
                    # we got a new title, so kill the aktual PID, if it is running
                    is_process_running(self.id_akt)
                    self.title_akt = title
                    self.artist_akt = artist
                    if self.album_akt == '':
                        self.album_akt = self.artist_akt
                    # todo: if there is a function that fetch the album name in under 1 Sec. it could be here.
                    # at the moment I move this at the end to the tagging method
                    # lookup album takes time, so we need to do it here (after arecord is started!)
                    # self.album_akt = get_albumname(self.artist_akt, self.title_akt, without='')

                    soundfile = self.title_akt + " - " + self.artist_akt + ".mp3"
                    if self.counttitles:
                        self.title_counter += 1
                        soundfile = format(self.title_counter, "03d") + " - " + soundfile
                    # update our window text
                    self.window["k_song"].update(self.td["k_song"] + soundfile)
                    self.window["k_title"].update(self.td["k_title"] + self.title_akt)
                    self.window["k_artist"].update(self.td["k_artist"] + self.artist_akt)
                    # kill old record befor new is started
                    # maybe this is double, but better than nothing
                    kill_all_arecords()
                    # start the new recording but only if started flag is set!
                    if self.started:
                        self.possible_recording = True
                        self.id_last = self.id_akt
                        self.id_akt = self.record(soundfile)

                        # put the PID in the PID List
                        self.all_pids.append(self.id_akt)
                        # make a row of all file information and store it in the all_files list
                        # that's for präprocessing it later like mp3tagging or replaygain
                        row = {"filename": soundfile, "title": self.title_akt, "artist": self.artist_akt,
                               "album": self.album_akt, "path": self.filepath}
                        self.all_files.append(row)
                        # and show a notification
                        if self.notification:
                            Sg.SystemTray.notify(self.td2["notify1"],
                                                 self.td2["notify2"].format(self.artist_akt, self.title_akt,
                                                                            self.album_akt))
                else:
                    # get albumname takes much time, so I only call that once per song!
                    if not (self.title_old + self.artist_old == self.title_akt + self.artist_akt):
                        # time to do something, just playing
                        self.album_akt = get_albumname(self.artist_akt, self.title_akt, without='')
                        logging.info("Album got: ", self.album_akt)
                        if self.album_akt == '':
                            self.album_akt = self.artist_akt
                        # update our list of songs
                        for each in self.all_files:
                            if each["path"] == self.filepath and each["title"] == self.title_akt:
                                each["album"] = self.album_akt
                                break
                        # update our window text
                        self.window["k_album"].update(self.td["k_album"] + self.album_akt)
                        self.title_old = self.title_akt
                        self.artist_old = self.artist_akt
                    else:
                        # time to do another thing
                        pass
            else:
                logging.debug(_("not found"))
                self.possible_recording = False

        self.window.close()
        is_process_running(self.id_akt)


# check if a PID is running ,and if, terminate it
def is_process_running(process_id):
    """
        :return: a string
    """
    errors = False
    if process_id >= 0:
        try:
            os.kill(process_id, signal.SIGTERM)
        except OSError:
            errors = True
    return errors


# check all PID in a list, and terminate all except one that's given
def kill_all_except(notthis, list_of_pids):
    """
        :return: a string
    """
    errors = False
    for pid in list_of_pids:
        if (pid > 0) and (pid != notthis):
            try:
                os.kill(pid, signal.SIGTERM)
            except OSError:
                errors = True
    return errors


# kill all running instances of the 'arecord' program
def kill_all_arecords():
    """
        :return: a string
    """
    if platform.system() == "Linux" or platform.system() == "Linux2":
        proc = Popen('killall arecord',
                     shell=True,
                     stdout=PIPE,
                     stderr=PIPE,
                     )
        while proc.poll() is None:
            # to dedbug this use: output = proc.stdout.readline()
            proc.stdout.readline()
        return True
    elif platform.system() == "Darwin":
        return True
        # MAC OS X
    elif platform.system() == "Windows":
        return True
        # Windows
    else:
        return True


def main():
    """
    """
    almanach[lang[0]].install()
    global debuglevel
    home = os.path.expanduser("~")
    home += os.sep + 'Music_Rec'
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', action='store_true', default=False, dest='subfolder',
                        help=_('Subfolder for Artist'))
    parser.add_argument('-n', action='store_true', default=True, dest='notification',
                        help=_('Make a System Notification on new recording'))
    parser.add_argument('-debug', action='store_true', default=False, dest='debug',
                        help=_('Print out debugging Infos'))
    parser.add_argument('-q', action='store', dest='quality', default='1', choices=('1', '2'),
                        help=_('Quality of Soundfile'))
    parser.add_argument('-t', action='store_true', default=False, dest='mp3tag',
                        help=_('Tag all the mp3 files at end of recording'))
    parser.add_argument('-l', action='store', default='de', dest='localisation',
                        help=_('Give your favourite localisation. (Two letter format)'))
    parser.add_argument('-g', action='store', dest='genere',
                        help=_('Do nothing'))
    parser.add_argument('-p', action='store', dest='lame_path', default='',
                        help=_('Show the path to lame program.'))
    parser.add_argument('-o', action="store", dest='outpath', default=home, type=str)
    parser.add_argument('-C', action='store_true', dest='counter', default=False,
                        help=_('Add a counter in front of the Title. E.g. for Audiobooks'))

    results = parser.parse_args()
    quality = results.quality
    lame_path = results.lame_path
    outpath = results.outpath
    genere = results.genere
    mp3tag = results.mp3tag
    counter = results.counter
    subfolder = results.subfolder
    notification = results.notification
    localisation = results.localisation
    if results.debug:
        debuglevel = logging.INFO
        logging.info(_('Debuginfo is on'))
    else:
        debuglevel = logging.DEBUG
    if len(localisation) > 2:
        localisation = 'en'
    almanach[localisation].install()

    print(_('usage: [--flags options] [Path] '))

    # check if executables are installed

    executes_needed = ['lame', 'mp3gain', 'arecord', 'killall']
    for execute_needed in executes_needed:
        if which(execute_needed) is None:
            Sg.popup_error(
                f'Executable is not present, please install with e.g. : sudo apt install {execute_needed} on a '
                f'debian-like system')

    recorder = MyRecorder(lame_path, outpath, genere, subfolder, mp3tag, quality, counter, notification, localisation)

    recorder.startrecord()


# Main body
if __name__ == '__main__':
    main()
